Vue.component('pokemon', {
  template: `
  <div class="card" :class="['card-' + this.pokemon.type]" style="width: 20rem;">
    <div class="card-overlay">
      <a class="card-overlay__text" href="">READ MORE</a>
    </div>
    <div class="card-top">
      <div class="poke-img-circle">
        <img class="poke-img" :src="this.pokemon.img"></img>
      </div>
    </div>
    <div class="card-block">
      <h6 class="card-subtitle text-center">#00{{this.pokemon.id}}</h6>
      <h4 class="card-title text-center">{{this.pokemon.name}}</h4>

      <div class="card-stats">
        <ul>
          <li><i class="fa fa-heart"></i><span>{{this.pokemon.hp}}</span></li>
          <li><i class="fa fa-shield"></i><span>{{this.pokemon.defense}}</span></li>
          <li><i class="fa fa-tachometer"></i><span>{{this.pokemon.speed}}</span></li>
          <li><i class="fa fa-gavel"></i><span>{{this.pokemon.attack}}</span></li>
        </ul>
      </div>
    </div>
  </div>
  `,
  props: ['pokeid'],

  data() {

    return {
      // pokemon_evolution: [],

      pokemon: {

        id: '',
        name: '',
        type: '',

        weight: '',
        height: '',

        hp: '',
        defense: '',
        speed: '',
        attack: '',

        img: '',
      }
    }
  },

  methods: {

    getData(id) {
      let req = 'http://pokeapi.co/api/v2/pokemon/'+id;
      axios.get(req).then(response => {

        this.pokemon.name = response.data.name;
        this.pokemon.type = response.data.types[0].type.name;

        this.pokemon.weight = response.data.weight;
        this.pokemon.height = response.data.height;

        this.pokemon.speed = response.data.stats[0].base_stat;
        this.pokemon.defense = response.data.stats[3].base_stat;
        this.pokemon.attack = response.data.stats[4].base_stat;
        this.pokemon.hp = response.data.stats[5].base_stat;

        this.pokemon.img = response.data.sprites.front_default;
      })

    },

    // getEvolutionChain(id) {
    //   let req = 'http://pokeapi.co/api/v2/evolution-chain/'+id;
    //   axios.get(req).then(response => this.pokemon_evolution = response.data);
    // }

  },

  mounted() {
    this.pokemon.id = this.pokeid;
    this.getData(this.pokemon.id);
  }


})

var app = new Vue({

  el: '#app',

  data: {
    pokeids: []
  },

  methods: {

    randomizeIDs() {
      this.pokeids = Array.from({length: 12}, () => Math.floor(Math.random() * 150) + 1);
    }

  },

  created() {
    this.randomizeIDs();
  }

});

var scroll = new Vue({
  el: '#scrollable'
})
